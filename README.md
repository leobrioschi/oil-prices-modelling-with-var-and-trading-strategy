# Oil prices modelling with VAR and trading strategy

Basing myself on previous work, specially by *Kilian* such as:
1. *Kilian, Lutz* (2009) : **Not all oil price shocks are alike: Disentangling demand and supply shocks in the crude oil market**. *American Economic Review*, 99(3), 1053-69.
2. *Kilian, Lutz* (2019) : **Facts and Fiction in Oil Market Modeling**, CESifo Working Paper, No. 7902, *Center for Economic Studies and ifo Institute (CESifo)*, Munich

>And quoting Kilian (2019) *"the relative importance of oil supply and oil demand shocks as drivers of the real price of oil is invariant to which admissible model solution one focuses on."*

As an excercise I am trying to model *Crude Oil Price* (or return) with a VAR model and some free data from **Nasdaq data link**.

At the end of the *VAR* model, I build a trading strategy based on a short stop-loss and the next day forecast. This strategy yields positive return in the testing data (*the whole pandemic years up to the Russian attack over Ukraine until 2022-06-02*) and in the backtest (*also a complicated era with the price jumping to 120 down afterwards to 20 USD*).
> For the sake of simplicity, it only trades at *Closing* prices


## About

I am Leonardo Brioschi, a Ph.D. Candidate in Accounting/Finance. Please visit https://leobrioschi.gitlab.io and contact me! 